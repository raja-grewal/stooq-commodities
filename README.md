# Archive of Stooq Commodity Prices

[![License](https://img.shields.io/badge/License-MIT-green)](https://codeberg.org/raja-grewal/stooq-commodities/src/branch/main/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://pre-commit.com/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

Due to the known [issue](https://github.com/pydata/pandas-datareader/issues/925) of no longer be able to download of commodity prices directly from the [Stooq](https://stooq.com/) API using [pandas-datareader](https://pandas-datareader.readthedocs.io/en/latest/index.html), this repository stores a limited archive of previously obtained data.

The generation of historical data is done using `gen_data.py` with customisation options regarding asset selection and dates available within the file. Tests for all user inputs have also been written.

We provide seven sets of databases outlined in the table below. Note the count refers to cleaned data which only includes days where prices are available for all included assets.

These include the equity indices S\&P500 (SPX), Nasdaq 100 (NDX), Dow Jones Industrial Average (DJIA), along components of the DJIA. For commodities, we provide front month futures contract prices for gold, silver, high grade copper, platinum, palladium, WTI crude oil, RBOB gasoline, lumber, live cattle, coffee, and orange juice. The included 25 of total 30 DJIA components in DJI are not exactly the 26 in Full due to additions and removals to the index.

|  | Components | Count | Start Date | End Date |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| **Equity Indices**
| SNP | SPX | 9,167 | 1985-10-01 | 2022-02-10 |
| USEI | SPX + NDX, DJIA | 9,167 | 1985-10-01 | 2022-02-10 |
| DJI | USEI + 25 DJIA Components | 8,024 | 1990-03-26 | 2022-02-10 |
| **Broader Markets**
| Minor | USEI + Gold, Silver, WTI | 9,090 | 1985-10-01 | 2021-12-24 |
| Medium | Minor + Cooper, Platinum, Lumber | 9,077 | 1985-10-01 | 2021-12-24 |
| Major | Medium + Palladium, RBOB, Cattle, Coffee, OJ | 8,990 | 1985-10-01 | 2021-12-24 |
| Full | Major + 26 DJIA Components | 7,858 | 1990-03-26 | 2021-12-24 |

Overall, these consist of daily time series of prices spanning from the earlier of either the first available date or the 10th October 1985. Equity indices can be updated whereas Broader Markets are not updatable as explained in the [issue](https://github.com/pydata/pandas-datareader/issues/925).

Accessing the code involves having already installed [Git LFS](https://git-lfs.github.com/) and then running the following commands:
```
git clone https://codeberg.org/raja-grewal/stooq-commodities.git

cd stooq-commodities
```

Next enable Git LFS and then both `fetch` and `checkout` the most up-to-date existing data:
```
git lfs install

git lfs pull
```

Install all required packages (ideally in a virtual environment) without dependencies using:
```
pip install setuptools pip --upgrade

pip install pandas-datareader==0.10.0
```

Optionally, install and enable the use of [pre-commit](https://pre-commit.com/) for making contributions:
```
pip install pre-commit

pre-commit install
```

Historical financial market data is sourced and aggregated using:
```
python gen_data.py
```

The `.pkl` dataframes and `.csv` files are equivalent, while the `.npy` arrays are cleaned such that only dates where all assets have prices are included. The `.npy` files are also always ordered where earlier dates are indexed first, whereas the ordering in `.pkl` and `.csv` files is dependent on the communication between the Stooq API and relevant packages.

Additionally, due to requests we also provide the same data for each singular asset contained in the "DJI" and "Major" databases. Note that the arrays are not temporally synchronised across assets belonging to each set due to the positioning of removal of missing set values.

Copyright (C) 2022 - 2024 J. S. Grewal (rg_public@proton.me).
